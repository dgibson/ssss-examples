# SSSS Kit

A collection of simple implementations of Shamir's Secret Sharing Scheme.

## Details

All the examples are invoked from the command line with a list of
files containing shares of a secret file split according to
Shamir's Secret Sharing Scheme.  The filenames must have the format:
	name.XXX
Where XXX is a decimal value from 1 to 255.  All the files should have
the same length.

The secret derived from the shares (if there are enough of them) is
output in binary to standard output.

In all cases, the Shamir algorithm is implemented across GF(256) the
finite field of 256 elements.  By standard results in algebra, GF(256)
is isomorphic to the quotient ring of Z_2[x] with the irreducible
polynomial x^8 + x^4 + x^3 + x^2 + 1.  Thus each element in GF(256)
can be represented as an element of Z_2[x] of degree less than 8,
which is the basis of the representation used here.

Z_2[x] is the polynomial ring over the finite field of 2 elements,
Z_2.  This can be represented as an integer, with the LSB representing
the constant term, the next most significant bit the co-efficient of x
and so forth.

Each byte of the secret file is interpreted as an element of GF(256)
in this way, and separately split using Shamir's algorithm into
corresponding bytes in the share files.

## License

Written in 2023 by David Gibson <david@gibson.dropbear.id.au>

To the extent possible under law, the author(s) have dedicated all
copyright and related and neighboring rights to this software to the
public domain worldwide. This software is distributed without any
warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this software. If not, see
<http://creativecommons.org/publicdomain/zero/1.0/>.
