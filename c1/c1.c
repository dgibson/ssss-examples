/* SPDX-License-Identifier: CC0-1.0 */
/*
 * ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
 *
 * Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to
 * the public domain worldwide. This software is distributed without
 * any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <inttypes.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Irreducible Polynomial in Z_2[x] used for the representation:
 *	x^8 + x^4 + x^3 + x^2 + 1
 */
#define GF_POLY		((uint16_t) 0x11d)

/* A value in GF(256) */
typedef struct gf256 {
	uint8_t v;
} gf256_t;

/* Additive identity in GF(256) */
const gf256_t GF_ZERO = { .v = 0 };

/* Multiplicative identity in GF(256) */
const gf256_t GF_ONE = { .v = 1 };


static gf256_t gf256(uint8_t v)
{
	gf256_t x = { .v = v };
	return x;
}

/* Add @a and @b in GF(256) */
static gf256_t gf_add(gf256_t a, gf256_t b)
{
	gf256_t s = { .v = a.v ^ b.v };

	return s;
}

/* Subtract @b from @a in GF(256) */
static gf256_t gf_sub(gf256_t a, gf256_t b)
{
	return gf_add(a, b);
}

/* Multiply @a and @b in GF(256) */
static gf256_t gf_mul(gf256_t a, gf256_t b)
{
	/* Extend to 16-bits temporarily */
	uint16_t aa = a.v;
	uint16_t bb = b.v;
	uint16_t pp = 0;
	int i;

	/* Compute the product in Z_2[x] (which may be up to degree 14) */
	for (i = 0; i < 8; i++) {
		if (aa & (1 << i))
			pp ^= bb << i;
	}

	/* Reduce modulo GF_POLY */
	for (i = 7; i >= 0; i--) {
		if (pp & (1 << (i + 8)))
			pp ^= GF_POLY << i;
	}

	return gf256(pp);
}

/* Divide @a by @b in GF(256) */
static gf256_t gf_div(gf256_t a, gf256_t b)
{
	int i;

	assert(b.v != 0); /* Division by zero */

	/* Dumb slow implementation, but we only need to do it once.
	 * Just linear search att 255 non-zero values for a
	 * quotient q such that a = b * q */
	for (i = 1; i < 256; i++) {
		gf256_t q = gf256(i);

		if (a.v == gf_mul(b, q).v)
			return q;
	}

	/* Everything has a multiplicative inverse in a field */
	assert(0);
}

/* Evaluate a Lagrange basis polynomial at @x.  Specifically the
 * polynomial which is 1 at @xs[@j] and 0 at all other @xs[0..@n].
 */
static gf256_t lagrange_at_x(size_t n, const gf256_t *xs, int j, gf256_t x)
{
	gf256_t numerator = GF_ONE;
	gf256_t denominator = GF_ONE;
	int i;

	for (i = 0; i < n; i++) {
		if (i == j)
			continue;

		numerator = gf_mul(numerator, gf_sub(x, xs[i]));
		denominator = gf_mul(denominator, gf_sub(xs[j], xs[i]));
	}

	return gf_div(numerator, denominator);
}

/* Given @ls[] with the values at 0 of the Lagrange basis polynomials
 * for x coordinates x0..x(@n-1), return the value at 0 of the unique
 * polynomial which passes through points (x0, ys[0]) .. (x(@n-1),
 * ys[@n-1])
 */
static gf256_t interpolate_one(size_t n, const gf256_t *ls, const gf256_t *ys)
{
	gf256_t out = GF_ZERO;
	int i;

	for (i = 0; i < n; i++)
		out = gf_add(out, gf_mul(ls[i], ys[i]));

	return out;
}

/* Recombine the @n open files @inputs according to Shamir's Secret
 * Sharing Scheme.
 *
 * All the files should have equal length.  Each byte in the output is
 * computed separately from the corresponding bytes in the input
 * files.  A set of corresponding bytes in the input files gives the
 * y-coordinates for a single Lagrange interpolation.  The
 * x-coordinates are the same for every byte, and are in @xs.
 */
static void interpolate_files(size_t n, const gf256_t *xs,
			      FILE *const *inputs, FILE *output)
{
	gf256_t ls[n];
	int i;

	/* Precompute the values of the Lagrange basis polynomials at 0 */
	for (i = 0; i < n; i++)
		ls[i] = lagrange_at_x(n, xs, i, GF_ZERO);

	while (1) {
		/* The y-coordinates for the current byte */
		gf256_t ys[n];
		/* The recovered secret value for this byte */
		gf256_t ysecret;
		int c;

		for (i = 0; i < n; i++) {
			c = getc(inputs[i]);

			if (c == EOF) {
				/* EOF is expected, other errors are not */
				assert(feof(inputs[i]));
				return;
			}

			/* Assuming no EOF or error, we should have a
			 * single byte value */
			assert((c >= 0) && (c <= 0xff));

			ys[i] = gf256(c);
		}

		ysecret = interpolate_one(n, ls, ys);

		c = putc(ysecret.v, output);
		assert(c == ysecret.v);
	}
}

int main(int argc, char *argv[])
{
	size_t n;
	int i;
	gf256_t *xs;
	FILE **inputs;

	n = argc - 1;
	assert(n >= 2);

	xs = malloc(n * sizeof(gf256_t));
	assert(xs);

	inputs = malloc(n * sizeof(FILE *));
	assert(inputs);
	
	for (i = 0; i < n; i++) {
		char *filename = argv[i + 1];
		char *e;
		unsigned long xval;

		/* Each filename should have the format 'name.XXX'
		 * where XXX is the x-coordinate for this share, in
		 * decimal */
		xval = strtoul(filename + strlen(filename) - 3, &e, 10);
		assert(*e == '\0');
		assert(xval <= 0xff);

		xs[i] = gf256(xval);

		inputs[i] = fopen(filename, "rb");
		assert(inputs[i]);
	}

	interpolate_files(n, xs, inputs, stdout);
}
