#! /usr/bin/python3

# SPDX-License-Identifier: CC0-1.0
#
# ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
#
# Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
#
# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

import sys


# Irreducible Polynomial in Z_2[x] used for the representation:
#	x^8 + x^4 + x^3 + x^2 + 1
GF_POLY = 0x11d


# Additive identity in GF(256)
GF_ZERO = 0


# Multiplicative identity in GF(256)
GF_ONE = 1


# Verify value is a suitable GF(256) element and return it
def gf_check(v):
    if not isinstance(v, int):
        raise TypeError
    if v < 0 or v > 255:
        raise ValueError
    return v


# Multiply @a and @b in GF(256)
def gf_mul(a, b):
    a = gf_check(a)
    b = gf_check(b)
    p = GF_ZERO

    while a:
        if a & 1:
            p ^= b
        a >>= 1
        b <<= 1
        if b & 0x100:
            b ^= GF_POLY

    return p


# Divide @a by @b in GF(256)
def gf_div(a, b):
    a = gf_check(a)
    b = gf_check(b)

    if b == GF_ZERO:
        raise ZeroDivisionError

    # Dumb slow implementation, but we only need to do it once.  Just
    # linear search att 255 non-zero values for a quotient q such that
    # a = b * q */
    for q in range(1, 256):
        if a == gf_mul(b, q):
            return q

    # Everything has a multiplicative inverse in a field
    assert False


# Evaluate a Lagrange basis polynomial at @x.  Specifically the
# polynomial which is 1 at @x[@j] and 0 at all other @x[_].
def lagrange(shares, j, x):
    xs = [gf_check(xi) for (xi, _) in shares]

    xj = gf_check(xs[j])
    del xs[j]

    n = d = GF_ONE
    for xi in xs:
        n = gf_mul(n, x ^ xi)
        d = gf_mul(d, xj ^ xi)

    return gf_div(n, d)


def ssss_combine(shares):
    # Precompute the values of the Lagrange polynomials at x==0
    ls = [lagrange(shares, j, GF_ZERO) for j in range(0, len(shares))]

    # Check all the input shares have the same length
    datalen = len(shares[0][1])
    for _, data in shares:
        if len(data) != datalen:
            raise ValueError

    output = bytearray()
    for n in range(0, datalen):
        # The y-coordinates for the current byte
        ys = [gf_check(data[n]) for (_, data) in shares]

        o = GF_ZERO
        for li, yi in zip(ls, ys):
            o = o ^ gf_mul(li, yi)
        output.append(o)

    return bytes(output)


if __name__ == '__main__':
    shares = []
    for name in sys.argv[1:]:
        # Extract x co-ordinate from the filename
        x = int(name[-3:])
        # Read the share data
        data = open(name, "rb").read()
        shares.append((x, data))

    output = ssss_combine(shares)
    sys.stdout.buffer.write(output)
