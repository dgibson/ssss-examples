#! /usr/bin/env python3

import sys
import os
import subprocess


MIN_SHARES = 2


def combos(shares):
    if not shares:
        yield ()
    else:
        for c in combos(shares[1:]):
            yield c
            yield (shares[0],) + c


def check_combo(combiner, secret, threshold, cshares):
    if len(cshares) < MIN_SHARES:
        return

    cmd = "{} {}".format(combiner, " ".join(cshares))
    proc = subprocess.run(cmd, capture_output=True, shell=True)
    try:
        proc.check_returncode()
    except subprocess.CalledProcessError:
        print("Recombination error", file=sys.stderr)
        sys.stdout.buffer.write(proc.stderr)
        sys.exit(1)
    if len(cshares) < threshold:
        if proc.stdout == secret:
            print("Reconstructed secret with less than threshold: {}".format(cshares), file=sys.stderr)
            sys.exit(2)
    else:
        if proc.stdout != secret:
            print("Failed to reconstruct secret with threshold shares: {}".format(cshares), file=sys.stderr)
            sys.stdout.buffer.write(proc.stderr)
            sys.exit(2)


def main(combiner, secret, threshold, *shares):
    threshold = int(threshold)
    secret = open(secret, "rb").read()

    for c in combos(shares):
        check_combo(combiner, secret, threshold, c)


if __name__ == '__main__':
    try:
        combiner = os.environ['COMBINER']
    except KeyError:
        combiner = "gfcombine -o-"

    main(combiner, *sys.argv[1:])
