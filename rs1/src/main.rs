//! SPDX-License-Identifier: CC0-1.0
//!
//! ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
//!
//! Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
//!
//! To the extent possible under law, the author(s) have dedicated all
//! copyright and related and neighboring rights to this software to
//! the public domain worldwide. This software is distributed without
//! any warranty.
//!
//! You should have received a copy of the CC0 Public Domain Dedication
//! along with this software. If not, see
//! <http://creativecommons.org/publicdomain/zero/1.0/>.

use std::io::{stdout, Write};
use std::ops::{Add, Div, Mul, Sub};

/// Represents values in GF(256)
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct GF256(u8);

impl GF256 {
    /// Irreducible Polynomial in Z_2[x] used for the representation:
    ///    x^8 + x^4 + x^3 + x^2 + 1
    const POLY: u16 = 0x11d;

    /// Additive identity
    pub const ZERO: Self = Self(0);

    /// Multiplicative identity
    pub const ONE: Self = Self(1);

    /// Construct a GF256 from a byte value
    pub const fn new(b: u8) -> Self {
        GF256(b)
    }

    /// Get the underlying byte representing the value
    pub const fn as_byte(self) -> u8 {
        self.0
    }
}

impl Add for GF256 {
    type Output = Self;
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn add(self, rhs: Self) -> Self {
        GF256(self.0 ^ rhs.0)
    }
}

impl Sub for GF256 {
    type Output = Self;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn sub(self, rhs: Self) -> Self {
        self + rhs
    }
}

impl Mul for GF256 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
	// Extend to 16-bits temporarily
        let mut p = 0u16;
        let a = self.0 as u16;
        let b = rhs.0 as u16;

	// Compute the product in Z_2[x] (which may be up to degree 14)
        for i in 0..8 {
            if (a & (1 << i)) != 0 {
                p ^= b << i;
            }
        }

        // Reduce modulo Self::POLY
        for i in (0..8).rev() {
            if (p & (1 << (i + 8))) != 0 {
                p ^= (Self::POLY) << i;
            }
        }

        assert!((p >> 8) == 0);
        GF256(p as u8)
    }
}

impl Div for GF256 {
    type Output = Self;
    fn div(self, rhs: Self) -> Self {
        // Really slow and stupid div implementation, but we don't
        // need to do it often
        for q in 1..=255 {
            let q = GF256(q);
            if rhs * q == self {
                return q;
            }
        }

	/* Everything has a multiplicative inverse in a field */
        unreachable!();
    }
}

fn shamir_combine(shares: &[Share]) -> Vec<u8> {
    // Precompute the values of the Lagrange polynomials at x==0
    let mut lat0 = Vec::new();
    for i in 0..shares.len() {
        let mut l = GF256::ONE;

        for j in 0..shares.len() {
            if j == i {
                continue;
            }

            l = l * shares[j].x / (shares[j].x - shares[i].x);
        }
        lat0.push(l);
    }

    // Check all the input shares have the same length
    let datalen = shares[0].data.len();
    for share in shares.iter().skip(1) {
        assert_eq!(share.data.len(), datalen);
    }

    let mut output = Vec::new();

    for i in 0..datalen {
        // The y-coordinates for the current byte
        let ys: Vec<_> = shares.iter().map(|s| GF256::new(s.data[i])).collect();
        let mut p = GF256::ZERO;

        for i in 0..ys.len() {
            p = p + lat0[i] * ys[i];
        }

        output.push(p.as_byte());
    }

    assert_eq!(output.len(), datalen);
    output
}

struct Share {
    x: GF256,
    data: Vec<u8>,
}

fn main() {
    let mut sharedata = Vec::new();

    for sharename in std::env::args().skip(1) {
        // Extract x co-ordinate from the filename
        let xn = sharename.rsplit_once('.').expect("Unexpected filename").1;
        // Parse it into an integer
        let x: u8 = xn.parse().expect("Couldn't parse filename");

        // Read the share data
        let data = std::fs::read(sharename).expect("Couldn't read share data");

        sharedata.push(Share { x: GF256(x), data });
    }

    let secret = shamir_combine(&sharedata);

    stdout().write_all(&secret).unwrap();
}
