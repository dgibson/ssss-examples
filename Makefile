# SPDX-License-Identifier: CC0-1.0
#
# ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
#
# Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
#
# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

SECRET = example-secret.txt
THRESHOLD = 3
SHARES = $(wildcard example-secret.txt.*)
COMBINERS = gfcombine c1 c2 c1static c2static rs1 rs2 py1 py2

COMBINE_C1 = ./c1/c1
COMBINE_C2 = ./c2/c2
COMBINE_RS1 = ./rs1/target/release/rs1
COMBINE_RS2 = ./rs2/target/release/rs2
COMBINE_PY1 = ./py1/py1.py
COMBINE_PY2 = ./py2/py2.py

CHECKER = ./check-recombination.py

.PHONY: all
all: $(COMBINE_C1) $(COMBINE_C2) $(COMBINE_RS1) $(COMBINE_RS2)

.PHONY: static
static: $(COMBINE_C1).static $(COMBINE_C2).static

.PHONY: check check-%
check: $(COMBINERS:%=check-%)

check-gfcombine: $(CHECKER)
	$(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-c1: $(COMBINE_C1) $(CHECKER)
	COMBINER="$(COMBINE_C1)" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-c2: $(COMBINE_C2) $(CHECKER)
	COMBINER="$(COMBINE_C2)" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-c1static : $(COMBINE_C1).static $(CHECKER)
	COMBINER="$(COMBINE_C1).static" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-c2static: $(COMBINE_C2).static $(CHECKER)
	COMBINER="$(COMBINE_C2).static" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-rs1: $(COMBINE_RS1) $(CHECKER)
	COMBINER="$(COMBINE_RS1)" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-rs2: $(COMBINE_RS2) $(CHECKER)
	COMBINER="$(COMBINE_RS2)" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-py1: $(COMBINE_PY1) $(CHECKER)
	COMBINER="python3 $(COMBINE_PY1)" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

check-py2: $(COMBINE_PY2) $(CHECKER)
	COMBINER="python3 $(COMBINE_PY2)" $(CHECKER) $(SECRET) $(THRESHOLD) $(SHARES)

.PHONY: time
bench: all bench-secret
	time gfcombine -ooutput-bench-gfcombine bench-secret.*
	cmp output-bench-gfcombine bench-secret
	time $(COMBINE_C1) bench-secret.* > output-bench-c1
	cmp output-bench-c1 bench-secret
	time $(COMBINE_C2) bench-secret.* > output-bench-c2
	cmp output-bench-c2 bench-secret
	time $(COMBINE_RS1) bench-secret.* > output-bench-rs1
	cmp output-bench-rs1 bench-secret
	time $(COMBINE_RS2) bench-secret.* > output-bench-rs2
	cmp output-bench-rs2 bench-secret
	time python3 $(COMBINE_PY1) bench-secret.* > output-bench-py1
	cmp output-bench-py1 bench-secret
	time python3 $(COMBINE_PY2) bench-secret.* > output-bench-py2
	cmp output-bench-py2 bench-secret

BENCH_SIZE = 8

bench-secret:
	dd if=/dev/urandom bs=1M count=$(BENCH_SIZE) of=$@
	gfsplit -n3 -m3 $@

$(COMBINE_RS1): FORCE
	cd rs1 && cargo build --release

$(COMBINE_RS2): FORCE
	cd rs2 && cargo build --release

./c1/%: FORCE
	$(MAKE) -C c1 $*

./c2/%: FORCE
	$(MAKE) -C c2 $*

clean:
	rm -f *~ output-* bench-secret*
	make -C c1 clean
	make -C c2 clean
	cd rs1 && cargo clean
	rm -f rs1/*~
	cd rs2 && cargo clean
	rm -f rs2/*~
	rm -f py1/*~
	rm -f py2/*~

.PHONY: FORCE
FORCE:
