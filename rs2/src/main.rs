// SPDX-License-Identifier: CC0-1.0
//
// ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
//
// Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to
// the public domain worldwide. This software is distributed without
// any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication
// along with this software. If not, see
// <http://creativecommons.org/publicdomain/zero/1.0/>.

use std::io::{stdout, Write};
use std::ops::{Add, Div, Mul, Sub};

/// Represents values in GF(256)
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct GF256(u8);

/// Table of discrete logarithms (and antilogs) in GF(256)
struct LogTable {
    /// exp[i] is the base raised to the i-th power.
    exp: [GF256; 255],
    /// base raised to power of log[v-1] GF256(v)
    log: [u8; 255],
}

impl GF256 {
    /// Irreducible Polynomial in Z_2[x] used for the representation:
    /// x^8 + x^4 + x^3 + x^2 + 1 */
    const POLY: u16 = 0x11d;

    /// Primitive element (multiplicative generator)
    /// x^1 + 0
    #[allow(dead_code)]
    const PRIM: Self = Self(0x2);

    /// logs and exponentials for base PRIM
    const TABLE: LogTable = Self::gen_table();

    /// Additive identity
    pub const ZERO: Self = Self(0);

    /// Multiplicative identity
    pub const ONE: Self = Self(1);

    /// Construct a GF256 from a byte value
    pub const fn new(b: u8) -> Self {
        GF256(b)
    }

    /// Get the underlying byte representing the value
    pub const fn as_byte(self) -> u8 {
        self.0
    }

    /// Calculate log and exponential tables
    const fn gen_table() -> LogTable {
        let mut exp = [Self::ONE; 255];
        let mut log = [0; 255];
        let mut i = 0;
	// Self::PRIM  raised to the i-th power */
        let mut gtoi = 1u16;

        // Can't use for loops in a const function
        while i < 254 {
            i += 1;
	    // Multiply current value by (x), which is equivalent to a
	    // shift left, then reducing moduloSelf::POLY
            gtoi <<= 1;
            if (gtoi & 0x100) != 0 {
                gtoi ^= Self::POLY;
            }
            exp[i] = Self(gtoi as u8);
            log[gtoi as usize - 1] = i as u8;
        }
        LogTable { exp, log }
    }

    // Return Self::PRIM to the @self-th power
    pub fn exp(n: u8) -> Self {
        let n = n % 255;
        Self::TABLE.exp[n as usize]
    }

    // Returns the discrete logarithm of @self, base Self::PRIM
    pub fn log(self) -> Option<u8> {
        if self.0 == 0 {
            None
        } else {
            Some(Self::TABLE.log[self.0 as usize - 1])
        }
    }

}

impl Add for GF256 {
    type Output = Self;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn add(self, rhs: Self) -> Self {
        GF256(self.0 ^ rhs.0)
    }
}

impl Sub for GF256 {
    type Output = Self;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn sub(self, rhs: Self) -> Self {
        self + rhs
    }
}

impl Mul for GF256 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        match (self.log(), rhs.log()) {
            (Some(loga), Some(logb)) => {
                // Both @self and @rhs are non-zero
                // Extend to i16 so the addition doesn't overflow
                let logp = loga as u16 + logb as u16;
                Self::exp((logp % 255) as u8)
            }
            _ => Self::ZERO,
        }
    }
}

impl Div for GF256 {
    type Output = Self;

    fn div(self, rhs: Self) -> Self {
        match (self.log(), rhs.log()) {
            (Some(loga), Some(logb)) => {
                // Both @self and @rhs are non-zero
                // Extend to u16 and offset so we don't overflow
                let logq = 255 + loga as u16 - logb as u16;
                Self::exp((logq % 255) as u8)
            }
            (None, Some(_)) => Self::ZERO,
            _ => panic!(), // Division by zero
        }
    }
}

fn shamir_combine(shares: &[Share]) -> Vec<u8> {
    // Precompute the values of the Lagrange polynomials at x==0
    let mut lat0 = Vec::new();
    for i in 0..shares.len() {
        let mut l = GF256::ONE;

        for j in 0..shares.len() {
            if j == i {
                continue;
            }

            l = l * shares[j].x / (shares[j].x - shares[i].x);
        }
        lat0.push(l);
    }

    // Check all the input shares have the same length
    let datalen = shares[0].data.len();
    for share in shares.iter().skip(1) {
        assert_eq!(share.data.len(), datalen);
    }

    let mut output = Vec::new();

    for i in 0..datalen {
        // The y-coordinates for the current byte
        let ys: Vec<_> = shares.iter().map(|s| GF256::new(s.data[i])).collect();
        let mut p = GF256::ZERO;

        for i in 0..ys.len() {
            p = p + lat0[i] * ys[i];
        }

        output.push(p.as_byte());
    }
    assert_eq!(output.len(), datalen);
    output
}

struct Share {
    x: GF256,
    data: Vec<u8>,
}

fn main() {
    let mut sharedata = Vec::new();

    for sharename in std::env::args().skip(1) {
        // Extract x co-ordinate from the filename
        let xn = sharename.rsplit_once('.').expect("Unexpected filename").1;
        // Parse it into an integer
        let x: u8 = xn.parse().expect("Couldn't parse filename");

        // Read the share data
        let data = std::fs::read(sharename).expect("Couldn't read share data");

        sharedata.push(Share { x: GF256(x), data });
    }

    let secret = shamir_combine(&sharedata);

    stdout().write_all(&secret).unwrap();
}
