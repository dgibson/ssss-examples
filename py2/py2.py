#! /usr/bin/python3

# SPDX-License-Identifier: CC0-1.0
#
# ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
#
# Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
#
# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide. This software is distributed without any
# warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication
# along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

import sys


# Irreducible Polynomial in Z_2[x] used for the representation:
#    x^8 + x^4 + x^3 + x^2 + 1
GF_POLY = 0x11d


# Primitive element (multiplicative generator)
#    x^1 + 0
GF_PRIM = 0x2


# Additive identity in GF(256)
GF_ZERO = 0


# Multiplicative identity in GF(256)
GF_ONE = 1


# Verify value is a suitable GF(256) element and return it
def gf_check(v):
    if not isinstance(v, int):
        raise TypeError
    if v < 0 or v > 255:
        raise ValueError
    return v


# Generate and return log and anitlog tables (base GF_PRIM)
def gf_gen_table():
    exp = [1]
    log = [None] * 256
    # GF_PRIM raised to the i-th power
    gtoi = 1

    for i in range(1, 256):
	# Multiply current value by GF_PRIM == (x), which is
	# equivalent to a shift left, then reducing modulo GF_POLY
        gtoi <<= 1
        if gtoi & 0x100:
            gtoi ^= GF_POLY
        exp.append(gf_check(gtoi))
        log[gtoi] = i

    assert len(exp) == 256
    assert len(log) == 256

    return exp, log


EXP_TABLE, LOG_TABLE = gf_gen_table()


# Return GF_PRIM to the @n-th power
def gf_exp(n):
    if not isinstance(n, int):
        raise TypeError
    return EXP_TABLE[n % 255]


# Returns the discrete logarithm of @v, base GF_PRIM
def gf_log(v):
    v = gf_check(v)
    return LOG_TABLE[v]


# Evaluate the discrete log of the Lagrange basis polynomial at @x.
# Specifically the polynomial which is 1 at @x[@j] and 0 at all
# other @x[_].
def log_lagrange(shares, j, x):
    xs = [gf_check(xi) for (xi, _) in shares]

    xj = gf_check(xs[j])
    del xs[j]

    logn = logd = 0
    for xi in xs:
        logn += gf_log(x ^ xi)
        logd += gf_log(xj ^ xi)

    return (logn - logd) % 255


def ssss_combine(shares):
    # Precompute logs of the values of the Lagrange polynomials at
    # x==0
    logls = [log_lagrange(shares, j, GF_ZERO) for j in range(0, len(shares))]

    # Check all the input shares have the same length
    datalen = len(shares[0][1])
    for _, data in shares:
        if len(data) != datalen:
            raise ValueError

    output = bytearray()
    for n in range(0, datalen):
        # The y-coordinates for the current byte
        ys = [gf_check(data[n]) for (_, data) in shares]

        o = GF_ZERO
        for logl, y in zip(logls, ys):
            if y != GF_ZERO:
                o = o ^ gf_exp(logl + gf_log(y))
        output.append(o)

    return bytes(output)


if __name__ == '__main__':
    shares = []
    for name in sys.argv[1:]:
        # Extract x co-ordinate from the filename
        x = int(name[-3:])
        # Read the share data
        data = open(name, "rb").read()
        shares.append((x, data))

    output = ssss_combine(shares)
    sys.stdout.buffer.write(output)
