/* SPDX-License-Identifier: CC0-1.0 */
/*
 * ssss-examples - Simple implementations of Shamir's Secret Sharing Scheme
 *
 * Written in 2023 by David Gibson <david@gibson.dropbear.id.au>
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to
 * the public domain worldwide. This software is distributed without
 * any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <inttypes.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Irreducible Polynomial in Z_2[x] used for the representation:
 *	x^8 + x^4 + x^3 + x^2 + 1
 */
#define GF_POLY		((uint16_t) 0x11d)

/* A value in GF(256) */
typedef struct gf256 {
	uint8_t v;
} gf256_t;

/* Additive identity in GF(256) */
const gf256_t GF_ZERO = { .v = 0 };

/* Multiplicative identity in GF(256) */
const gf256_t GF_ONE = { .v = 1 };

static gf256_t gf256(uint8_t v)
{
	gf256_t x = { .v = v };
	return x;
}

/* Add @a and @b in GF(256) */
static gf256_t gf_add(gf256_t a, gf256_t b)
{
	gf256_t s = { .v = a.v ^ b.v };

	return s;
}

/* Add @a and @b in GF(256) */
static gf256_t gf_sub(gf256_t a, gf256_t b)
{
	return gf_add(a, b);
}

/*
 * Table of exponentials in GF(256)
 *
 * exp_table[i] is the element gf256(0x02) raised to the i-th power.
 *
 * 0x02 (the polynomial x == x^1 + 0) is a primitive element in
 * GF(256) with this representation, so every non-zero value will
 * appear in the table.
 */
static gf256_t exp_table[255];

/*
 * Table of discrete logarithms in GF(256)
 *
 * For the element gf256(v), x raised to the power of log_table[v-1]
 * is equal to gf256(v).
 */
static uint8_t log_table[255];

/* Generate the exponential and logarithm tables */
static void gf_gen_table(void)
{
	int i = 0;
	/* The generator (x) raised to the i-th power */
	uint16_t gtoi = 1;

	/* Anything to the zeroth power is one */
	exp_table[0] = GF_ONE;

	for (i = 1; i < 255; i++) {
		/* Multiply current value by (x), which is equivalent
		 * to a shift left, then reducing module GF_POLY
		 */
		gtoi <<= 1;
		if (gtoi & 0x100)
			gtoi ^= GF_POLY;

		exp_table[i] = gf256(gtoi);
		log_table[gtoi - 1] = i;
        }

	assert(i == sizeof(exp_table));
}

/* Returns gf256(0x2) to the @n-th power */
static gf256_t gf_exp(unsigned n)
{
	return exp_table[n % 255];
}

/* Returns the discrete logarithm of @v, base gf256(0x2) */
static unsigned gf_log(gf256_t v)
{
	assert(v.v != 0);
	return log_table[v.v - 1];
}

/* Evaluate the discrete log of the Lagrange basis polynomial at @x.
 * Specifically the polynomial which is 1 at @xs[@j] and 0 at all
 * other @xs[0..@n].
 */
static int log_lagrange_at_x(size_t n, const gf256_t *xs, int j, gf256_t x)
{
	unsigned logl = 0;
	int i;

	for (i = 0; i < n; i++) {
		if (i == j)
			continue;

		logl += 255; /* Bias to prevent underflow */
		logl += gf_log(gf_sub(x, xs[i]));
		logl -= gf_log(gf_sub(xs[j], xs[i]));
	}

	return logl % 255;
}

/* Given @logls[] with the logs of the values at 0 of the Lagrange
 * basis polynomials for x coordinates x0..x(@n-1), return the value
 * at 0 of the unique polynomial which passes through points (x0,
 * ys[0]) .. (x(@n-1), ys[@n-1])
 */
static gf256_t interpolate_one(size_t n, const int *logls, const gf256_t *ys)
{
	gf256_t out = GF_ZERO;
	int i;

	for (i = 0; i < n; i++) {
		if (ys[i].v != 0)
			out = gf_add(out, gf_exp(logls[i] + gf_log(ys[i])));
	}

	return out;
}

/* Recombine the @n open files @inputs according to Shamir's Secret
 * Sharing Scheme.
 *
 * All the files should have equal length.  Each byte in the output is
 * computed separately from the corresponding bytes in the input
 * files.  A set of corresponding bytes in the input files gives the
 * y-coordinates for a single Lagrange interpolation.  The
 * x-coordinates are the same for every byte, and are in @xs.
 */
static void interpolate_files(size_t n, const gf256_t *xs, FILE *const *inputs, FILE *output)
{
	int logls[n];
	int i;

	/* Precompute the (log) values of the Lagrange basis
	 * polynomials at 0 */
	for (i = 0; i < n; i++)
		logls[i] = log_lagrange_at_x(n, xs, i, GF_ZERO);

	while (1) {
		/* The y-coordinates for the current byte */
		gf256_t ys[n];
		/* The recovered secret value for this byte */
		gf256_t ysecret;
		int c;

		for (i = 0; i < n; i++) {
			c = getc(inputs[i]);

			if (c == EOF) {
				/* EOF is expoected, other errors are not */
				assert(feof(inputs[i]));
				return;
			}

			/* Assuming no EOF or error, we should have a
			 * single byte value */
			assert((c >= 0) && (c <= 0xff));

			ys[i] = gf256(c);
		}

		ysecret = interpolate_one(n, logls, ys);

		c = putc(ysecret.v, output);
		assert(c == ysecret.v);
	}
}

int main(int argc, char *argv[])
{
	size_t n;
	int i;
	gf256_t *xs;
	FILE **inputs;

	gf_gen_table();

	n = argc - 1;
	assert(n >= 2);

	xs = malloc(n * sizeof(gf256_t));
	assert(xs);

	inputs = malloc(n * sizeof(FILE *));
	assert(inputs);
	
	for (i = 0; i < n; i++) {
		char *filename = argv[i + 1];
		char *e;
		unsigned long xval;

		/* Each filename should have the format 'name.XXX'
		 * where XXX is the x-coordinate for this share, in
		 * decimal */
		xval = strtoul(filename + strlen(filename) - 3, &e, 10);
		assert(*e == '\0');
		assert(xval <= 0xff);

		xs[i] = gf256(xval);

		inputs[i] = fopen(filename, "rb");
		assert(inputs[i]);
	}

	interpolate_files(n, xs, inputs, stdout);
}
